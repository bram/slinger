
# Foucault Pendulum image analysis

The `slinger.py` script analyses video captures of the Foucault Pendulum
in the Huygens building of the Radboud University.

We suspect a defect in the mechanic that swings the pendulum. It's observed that rotation of the the plane of oscillation is not constant. It seems to slow down at some point. By observing the pendulum
with a web cam, we might find out what's going on.

## Install

(Developed and tested on Ubuntu 22.04.)

``` bash
apt install python-tk ffmpeg
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

Create an empty csv file:
``` bash
echo "file,timestamp,angle" > slinger.csv
```

`slinger.py` will read the csv-file and append a new line for every processed video. On later runs,
it will skip the already processed files.


## Run
Crop videos in `videos` directory to speed up processing (requires ffmpeg):

``` bash
mkdir cropped
crop-videos videos cropped
```

Analyse cropped videos:

``` bash
. venv/bin/activate
python slinger.py -v cropped -o output
```

Pres `q` to quit.


## Results

The data from the csv-file can be plotted with `plotslinger.py`:

```
python plotslinger.py
```

Assuming its executed in the same directory as `slinger.csv`.

The result looks like:

![Figure 1](img/Figure_1.png)

The output images look like:

![Output image](img/output.jpg)
