import pandas as pd
from datetime import datetime
import csv
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
#headers = ['timestamp','angle']
df = pd.read_csv('slinger.csv')
print (df)
x = df['timestamp']

dates=[datetime.fromtimestamp(ts) for ts in x]
y = df['angle']

# plot
plt.scatter(dates,y)
# beautify the x-labels
#plt.gcf().autofmt_xdate()
plt.show()
