# track foucault pendulum

import argparse
import datetime
import glob
import math
import re

import cv2
import numpy as np
import pandas as pd
from sklearn import linear_model
from tqdm import tqdm

# Argument input
parser = argparse.ArgumentParser()  # creating a parser
parser.add_argument("-v", "--videodir", required=True, help="directory with videos")
parser.add_argument(
    "-o", "--outputdir", required=True, help="directory to write images to"
)
args = vars(parser.parse_args())  # parsing arguments

# place windows once
img = np.zeros((100, 100, 3), dtype=np.uint8)
cv2.imshow("ballfinder", img)
cv2.imshow("balltracker", img)

cv2.moveWindow("ballfinder", 50, 25)
cv2.moveWindow("balltracker", 750, 25)


def drawtext(frame, text):
    y0, dy = 20, 22
    color = (255, 0, 0)
    for i, line in enumerate(text.split("\n")):
        y = y0 + i * dy
        cv2.putText(frame, line, (5, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, color, 2)


def gettimestamp(filename):
    # regex = r"(?P<camera>.*)_(.*)-(?P<seq>[0-9]+)-(?P<year>[0-9]{4})(?P<month>[0-9]{2})(?P<day>[0-9]{2})(?P<hour>[0-9]{2})(?P<minute>[0-9]{2})(?P<second>[0-9]{2})\.(?P<file_extension>.*)"
    regex = r".*_.*-[0-9]+-(?P<year>[0-9]{4})(?P<month>[0-9]{2})(?P<day>[0-9]{2})(?P<hour>[0-9]{2})(?P<minute>[0-9]{2})(?P<second>[0-9]{2})\..*"

    date_re = re.compile(regex)
    date_str = " ".join(date_re.search(filename).groups())

    return datetime.datetime.strptime(date_str, "%Y %m %d %H %M %S")


def getangle(coordinates):
    X = [i[0] for i in coordinates]
    Y = [i[1] for i in coordinates]
    x = np.array(X).reshape((-1, 1))
    y = np.array(Y)
    reg.fit(x, y)  # perform linear regression
    angle = math.degrees(math.atan2(1, reg.coef_))
    return angle


def drawbox(frame, box):
    (x, y, w, h) = [int(v) for v in box]
    # Draw a rectangle around the object
    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)


def drawtrack(frame, coordinates):
    for i in range(len(coordinates)):
        if i > 0:
            # Draw the trajectory
            cv2.line(frame, coordinates[i - 1], coordinates[i], (255, 0, 0), 1)


def findBoxAndFrame(videostream):
    backSub = cv2.createBackgroundSubtractorMOG2(500, 16, False)
    minperimeter = 1000
    frameno = 0
    minframe = 100
    while True:
        ret, frame = videostream.read()
        frameno += 1
        if frame is None:
            break

        try:
            startcontour
        except:
            pass
        else:
            if frameno > 100:
                break

        if frameno > 200:
            print("still no contour found")
            Exception()

        # update the background model
        fgmask = backSub.apply(frame)

        contours, hierarchy = cv2.findContours(
            fgmask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE
        )

        if len(contours) > 0:
            # find the biggest area of the contour
            c = max(contours, key=cv2.contourArea)

            x, y, w, h = cv2.boundingRect(c)
            # draw the 'human' contour (in green)
            drawbox(fgmask, (x, y, w, h))

            ratio = float(w) / h
            # only evaluate square enough contours
            if ratio >= 0.9 and ratio <= 1.1:
                perimeter = 2 * (w + h)
                if perimeter >= 200:
                    if perimeter < minperimeter:
                        # TODO we want the smallest/most square contour
                        print(f"found smallest blob so far: {frameno} {perimeter}")
                        minperimeter = perimeter
                        minframe = frameno
                        startcontour = c
                        savedFrame = cv2.cvtColor(fgmask, cv2.COLOR_GRAY2RGB)

        # show the image
        cv2.imshow("ballfinder", fgmask)
        cv2.waitKey(1)

    # display selected frame
    box = cv2.boundingRect(startcontour)
    drawbox(savedFrame, box)
    cv2.imshow("ballfinder", savedFrame)
    return box, minframe


reg = linear_model.LinearRegression()
df = pd.read_csv("slinger.csv")

for video in tqdm(glob.glob(f"{args['videodir']}/*")):
    # did we process this video?
    if video in df["file"].values:
        print(f"skipping video {video}")
        continue

    print(f"processing video: {video}")

    # open video
    vs = cv2.VideoCapture(video)

    # determine box and startframe
    box, startframenumber = findBoxAndFrame(vs)

    # set frame pointer to frame with nicest contour
    _ = vs.set(cv2.CAP_PROP_POS_FRAMES, startframenumber - 1)

    coordinates = []

    print(f"contour bounding rectangle {box}")
    while True:
        # read next frame
        (grabbed, frame) = vs.read()  # Loop frame by frame
        currentframenumber = vs.get(cv2.CAP_PROP_POS_FRAMES)

        if frame is None:
            # no frames left, go to next video
            print("[INFO] Going to next video")
            break

        # initialize the tracker when on the frame of interest
        if currentframenumber == startframenumber:
            print("[INFO] reached frame of interest")
            tracker = cv2.legacy.TrackerCSRT_create()
            tracker.init(frame, box)

        success, box = tracker.update(frame)
        if success:
            drawbox(frame, box)
            (x, y, w, h) = [int(v) for v in box]
            coordinates.append((int(x + w / 2), int(y + h / 2)))
            drawtrack(frame, coordinates)
        lastframe = frame.copy()
        drawtext(frame, f"frame: {currentframenumber:.0f}")
        cv2.imshow("balltracker", frame)
        key = cv2.waitKey(1)

        if key == ord("q"):
            quit()

    timestamp = gettimestamp(video).timestamp()
    angle = getangle(coordinates)
    line = f"{video},{timestamp},{angle}\n"
    # Append a line to the csv file
    csv = open("slinger.csv", "a")
    csv.write(line)
    csv.close()

    # save picture
    if args["outputdir"]:
        drawtext(
            lastframe,
            f"datetime: {datetime.datetime.fromtimestamp(timestamp)}\nangle: {angle:.1f}",
        )
        cv2.imwrite(f"{args['outputdir']}/{timestamp}.jpg", lastframe)
        # cv2.imwrite("/tmp/test.jpg" , lastframe)

cv2.destroyAllWindows()
